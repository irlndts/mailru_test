package DBMail;

use Moose;
use DBI;


has 'dbname' => (
	is => 'rw', #access for read/write
	reader => 'getDBName',
	writer => 'setDBName',
	clearer=> 'deleteDBName',
	);

has 'dbh' => (
	is => 'rw', #access for read/write
	reader => 'getdbh',
	writer => 'setdbh',
	clearer=> 'deletedbh',
	);

sub getAllPersons{
	my $self = shift;

	unless ($self->checkConnection()){
		print STDERR "write person error: db connection error\n";
		return undef;
	}
	
	my $dbh = $self->getdbh();

	#error, can't prepare query
	my $sth = undef;
	unless($sth = $dbh->prepare("select name, email from users")){
		print STDERR "write person error: prepare error\n";
		print STDERR ((defined($DBI::errstr))? $DBI::errstr:''), "\n";
		return undef;	
	};

	unless($sth->execute()){
		print STDERR "write person error: ecexute error\n";
		print STDERR ((defined($DBI::errstr))? $DBI::errstr:''), "\n";
		return undef;	
	}
	my @persons = ();

	while (my $row = $sth->fetchrow_hashref()){
		my $person;
		$person->{name} = $row->{name};
		$person->{email} = $row->{email};
		push (@persons, $person);
	}

	$sth->finish;

	return @persons;
};

#write person to the DB
sub writePerson {
	my $self = shift;
	my $name = shift;
	my $email = shift;
	my $id = shift;
	unless ($self->checkConnection()){
		print STDERR "write person error: db connection error\n";
		return undef;
	}
	
	my $dbh = $self->getdbh();

	#error, can't prepare query
	my $sth = undef;
	unless($sth = $dbh->prepare("insert into users (name, email, id) values (?,?,?)")){
		print STDERR "write person error: prepare error\n";
		print STDERR ((defined($DBI::errstr))? $DBI::errstr:''), "\n";
		return undef;	
	};

	unless($sth->execute($name,$email, $id)){
		print STDERR "write person error: ecexute error\n";
		print STDERR ((defined($DBI::errstr))? $DBI::errstr:''), "\n";
		return undef;	
	}
	$dbh->commit;
	$sth->finish;

	return 1;
}

#delete person from the DB
sub deletePerson {
	my $self = shift;
	my $id = shift;
	unless ($self->checkConnection()){
		print STDERR "delete person error: db connection error\n";
		return undef;
	}
	
	my $dbh = $self->getdbh();

	#error, can't prepare query
	my $sth = undef;
	unless($sth = $dbh->prepare("delete from users where id =?")){
		print STDERR "delete person error: prepare error\n";
		print STDERR ((defined($DBI::errstr))? $DBI::errstr:''), "\n";
		return undef;	
	};

	unless($sth->execute($id)){
		print STDERR "delete person error: ecexute error\n";
		print STDERR ((defined($DBI::errstr))? $DBI::errstr:''), "\n";
		return undef;	
	}
	$dbh->commit;
	$sth->finish;

	return 1;
}

#check connection. If it's not connected, so... try to connect. 10 seconds timeout for reconnect  try
sub checkConnection  {
	my $self = shift;
	
	my $dbh = $self->getdbh()|| undef;
	my $dbname = $self->getDBName();

	my $repeattime = 10; #time foor recheck connect if it's not connected for unknown reasen

	# ping cheking
	while (!defined($dbh) || !$dbh->ping()) {
		if (defined($dbh)) {
			print STDERR "check_db_connect: NO db connect... restore\n";
			print STDERR ((defined($DBI::errstr))? $DBI::errstr:''), "\n";
			$dbh->disconnect;
			undef($dbh);
		};

		$dbh = DBI->connect("dbi:SQLite:dbname=$dbname","","",{AutoCommit => 0, RaiseError => 0, PrintError => 0});

# Провер после попытки соединения
		if (!defined($dbh) || !$dbh->ping()) {
			print STDERR "check_db_connect: can not restore db connect:\n";
			print STDERR ((defined($DBI::errstr))? $DBI::errstr:''), "\n";
			if (defined($repeattime) && ($repeattime > 0)) {
				sleep($repeattime);
			}
		};
	};
	$self->setdbh($dbh);
	return($dbh);

};




1;
