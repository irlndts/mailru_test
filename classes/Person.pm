package Person;
use Moose;
use classes::DBMail;
use strict;

#name of that person
has 'name' => (
	is => 'rw', #access for reed/write
	reader => 'getName', 
	writer => 'setName',
	clearer=> 'deleteName',
	predicate => 'hasName', #bool
	);

#email of that person
has 'email' => (
	is => 'rw',
	reader => 'getEmail',
	writer => 'setEmail',
	clearer=> 'deleteEmail',
	);

has 'id' => (
	is => 'rw',
	reader => 'getId',
	writer => 'setId',
	clearer=> 'deleteId',
	);

sub writePerson{
	my $self = shift;
	my $dbh = shift;
	unless ($dbh->writePerson($self->getName(),$self->getEmail(), $self->getId())){#write error
		return undef;
	}
	return 1;
}

sub deletePerson{
	my $self = shift;
	my $dbh = shift;
	unless ($dbh->deletePerson($self->getId())){#delete error
		return undef;
	}
	return 1;
}

1;
