#!/usr/bin/perl -w

use strict;
use Plack;
use Plack::Request;
use Plack::Response;
use classes::Person;
use classes::DBMail;
use classes::uri;
use Template;
use Data::Dumper;

#connect to database
my $dbh = new DBMail(dbname => 'db/mailru_test');

#template config 
my $template = Template->new({
		START_TAG       => quotemeta('<?'),
		END_TAG         => quotemeta('?>'),
		INTERPOLATE     => 0,
		AUTO_RESET      => 1,
		ERROR           => '_error',
		EVAL_PERL       => 1,
		CACHE_SIZE      => 1024,
		LOAD_PERL       => 1,
		RECURSION       => 1,
		INCLUDE_PATH    => './tmpl/',
		COMPILE_EXT     => '.tpl',
		COMPILE_DIR     => '/var/tmp/tt2cache',
		}) || die ('Create template error: '.Template->error());

my $app = sub {
	my $env = shift;
	my $req = Plack::Request->new($env);
	my $res;

	#get all GET parameters from request
	my $params = uri::getRequestParams(undef,$req->{env}->{QUERY_STRING});

	#get path from request
	my $path = $req->{env}->{PATH_INFO};
	
	my $cookies = undef;	
	#get cookies
	if (defined $req->{env}->{HTTP_COOKIE} && $req->{env}->{HTTP_COOKIE} ne ''){
		$cookies = $req->cookies;
	};


	my $res = Plack::Response->new(200);

	#user pushed log out button
	if (defined $params && defined $params->{status} && lc($params->{status}) eq 'logout' && defined $cookies){
		my $person = new Person(id => $cookies->{ID});
		$person->deletePerson($dbh);
		#clear cookies
		$res->cookies->{ID}={
			value => $cookies->{ID},
			expires => time,
		};
		$res->content_type('text/html');
		my %vars = undef;
		my $out = undef;
		$template->process('login.html',\%vars,\$out) ? $res->body($out):$res->body($template->error);

	}
	#if actual cookies are here - just show page for the user
	elsif (	defined $cookies && defined $cookies->{ID} && $cookies->{ID} ne ''){

		#get All Registered users
		my @users = $dbh->getAllPersons();
		
		my %vars = ();
	

		foreach (@users){
			$vars{'table'} .= "<tr><td>$_->{name}</td><td>$_->{email}</td></tr>";
		}
		

		$res->content_type('text/html');
		my $out = undef;
		$template->process('index.html',\%vars,\$out) ? $res->body($out):$res->body($template->error);

	#register user in system and set cookies for him
	}elsif (defined $params->{user} 
		&& $params->{user} ne ''
		&& defined $params->{email} 
		&& $params->{email} ne ''){
	
		#create cookie
		my $randomKey = join'', map +(0..9,'a'..'z','A'..'Z')[rand(10+26*2)], 1..8;
		$res->cookies->{ID} = {
			value => $randomKey,
			expires => time + 60 * 60,
		};
		
		#user tryes to login
		my $person = new Person(name => $params->{user}, email => $params->{email}, id => $randomKey);
		$person->writePerson($dbh);
		
		#get All Registered users
		my @users = $dbh->getAllPersons();
		
		my %vars = ();
	

		foreach (@users){
			$vars{'table'} .= "<tr><td>$_->{name}</td><td>$_->{email}</td></tr>";
		}
	
		#show page
		$res->content_type('text/html');
		my $out = undef;
		$template->process('index.html',\%vars,\$out) ? $res->body($out):$res->body($template->error);


	}
	else {
		#no cookies, ask user to login
		$res->content_type('text/html');
		my %vars = undef;
		my $out = undef;;
		$template->process('login.html',\%vars,\$out) ? $res->body($out):$res->body($template->error);

	}



	return $res->finalize();

};



